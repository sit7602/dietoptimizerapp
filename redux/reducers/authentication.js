import actionsNames from '../actions/actionsNames';

const initialState = {
  isAuthenticated: true,
  login: null,
  password: null,
  token: null,
  error: null,
  isLoading: false,
};

const authentication = (state = initialState, action) => {
  switch (action.type) {
    case actionsNames.START_AUTHENTICATION:
      return {
        ...state,
        error: null,
        isLoading: true,
      };
    case actionsNames.SET_AUTHENTICATED:
      return {
        ...state,
        login: action.login,
        password: action.password,
        token: action.token,
        isLoading: false,
      };
    case actionsNames.SET_ERROR:
      return {
        ...state,
        error: action.error,
        isLoading: false,
      };
    default:
      return state;
  }
};

export default authentication;
