import getUserInfoRequest from '../../network/getUserInfoRequest';
import {startAuthentication, setAuthenticated, setError} from './dumbActions';
import showSnackBar from '../../components/showSnackbar';

export const authenticate = token => {
  return async dispatch => {
    dispatch(startAuthentication());
    const {result, error} = await getUserInfoRequest(token);
    if (error !== null) {
      dispatch(setError(error));
      showSnackBar(error.message, 'SHORT', 'ОК', () => {});
      return;
    }
    dispatch(setAuthenticated(login, password, result.token));
  };
};
