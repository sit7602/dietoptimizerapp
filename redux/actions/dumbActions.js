import actionsNames from './actionsNames';

export const startAuthentication = () => ({
  type: actionsNames.START_AUTHENTICATION,
});

export const setAuthenticated = (login, password, token) => ({
  type: actionsNames.SET_AUTHENTICATED,
  login,
  password,
  token,
});

export const setError = error => ({
  type: actionsNames.SET_ERROR,
  error,
});

export const setUserInfo = (login, password, token) => ({
  type: actionsNames.SET_AUTHENTICATED,
  login,
  password,
  token,
});
