import React, { PureComponent } from 'react';
import { AppRegistry,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Modal,
} from 'react-native';
import { RNCamera } from 'react-native-camera';
import Tflite from 'tflite-react-native';
import { connect } from 'react-redux';
import { Button,
    Icon,
    TextInput,
    Title,
    ListView,
    TouchableNativeFeedback,
} from "@shoutem/ui";
import productSearchRequest from '../../network/productSearchRequest';
import showSnackBar from "../../components/showSnackbar";
import PopupDialog from "../../components/popup-dialog";
import Dialog, {
    DialogButton,
    DialogContent,
    DialogFooter,
    DialogTitle,
    FadeAnimation,
} from "react-native-popup-dialog";
const detectionIndexes = [439, 422,442,433, 506, 552, 900,901,902,903,930,945,946,949,936,939,942,952,953,955,958,959,961,965,967,968,969,];
// import CameraModal from "./cameraModal";
class Camera extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            token: '',
            barcodes: [],
            canDetectBarcode: false,
            detections: [],
            detectionsModalVisible: false,
            barcodesSearchResult: null,
            nnSearchResult: null,
            barcodesModalVisible: false,
            stackModalVisible: false,
            manualAddVisible: false,
            productsStack: [],
        };
        this.tflite = new Tflite();
    }


    componentDidMount() {
        this.tflite.loadModel({
            model: 'mobilenet_v1_1.0_224.tflite',//'mobilenet_v1_1.0_224_quant.tflite',
            labels: 'mobilenet_v1_1.0_224.txt',//'output.json',
            numThreads: 1,
        }, (err, res) => {
            if (err) {
                console.log('model load error');
                console.log(err);
            } else {
                console.log('model load success');
                console.log(res);
            }
        });
        const token = `Bearer_${this.props.token}`;
        this.setState({
            token: token,
        });
    }

    renderBarcodesModal = () => {
        if (this.state.barcodesSearchResult === null) {
            return null;
        }
        let calories = Math.round(
            this.state.barcodesSearchResult.calPer100Units * this.state.barcodesSearchResult.unitsPerPortion / 100
        );
        return (
            <View
                style={{
                    width: '100%',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}>
                <Title style={{marginBottom: 20}}>Вот что мы нашли</Title>
                <Text style={{fontSize: 18}}>Название: {this.state.barcodesSearchResult.name}</Text>

                <View style={styles.section__se}>
                    <View style={styles.col}>
                        <Text style={styles.value}>{calories}</Text>
                        <Text style={styles.key}>калории</Text>
                    </View>
                    <View style={styles.col}>
                        <Text style={styles.value}>{this.state.barcodesSearchResult.protein}</Text>
                        <Text style={styles.key}>белки</Text>
                    </View>
                    <View style={styles.col}>
                        <Text style={styles.value}>{this.state.barcodesSearchResult.fat}</Text>
                        <Text style={styles.key}>жиры</Text>
                    </View>
                    <View style={styles.col}>
                        <Text style={styles.value}>{this.state.barcodesSearchResult.carb}</Text>
                        <Text style={styles.key}>углеводы</Text>
                    </View>
                </View>
            </View>
        );
    };

    addBarcodeProduct = () => {
        // console.log(this.state.productsStack);
        const productList = this.state.productsStack.map(p=>{
            const pr = {
                numberOfPortions:1,
                selectedProduct: p,
                unitsInPortion: p.unitsPerPortion
            };
            return pr;
        });
        console.log(productList);
        // return
        this.props.history.push({
            pathname: '/product-form',
            state: {
                productList: productList,
            },
        });
    };

    addToStack = () => {
        let productsStack = this.state.productsStack;
        let res = this.state.barcodesSearchResult;
        res.index = new Date().getTime();
        productsStack.push(res);
        this.setState({productsStack: productsStack, barcodesModalVisible: false,});
        console.log(this.state.productsStack.length);
    };

    addNNProduct = () => {
        this.props.history.push({
            pathname: '/product-form',
            state: {
                selectedProduct: this.state.nnSearchResult,
            }
        });
    };

    renderProductsStackCounter = () => {
        if (this.state.productsStack.length > 0) {
            console.log('enter if');
            return (
                <View style={styles.productsStackCounter}>
                    <TouchableOpacity style={styles.productsStackBtn} onPress={() => {
                        this.setState({stackModalVisible: true});
                    }}>
                        <Text style={{
                            fontSize: 18,
                            alignItems: 'center',
                            paddingLeft: 7,
                            paddingRight: 7,
                            justifyContent: 'center'
                        }}>{this.state.productsStack.length}</Text>
                    </TouchableOpacity>
                </View>
            );
        }
        return null;
    };

    renderProductEditRow = (data) => {
        console.log(this.state.productsStack);
        if (this.state.productsStack.length > 0) {
            return (
                <View style={{width: '100%', height: 40, display: 'flex', flexDirection: 'row',}}>
                    <View style={{ width: '100%', height: 40, flexDirection: 'row', justifyContent: 'flex-start'}}>
                        <Text style={{
                            fontSize: 22,
                            marginLeft: 4,
                            // verticalAlign: 'middle',
                            // alignItems:'flex-start'
                            position: 'absolute',
                            left: 4,
                            top: 5,
                        }}>{data.name}</Text>
                        <Button
                            style={{position: 'absolute', right: 0, top: 10}}
                            styleName="clear"
                            onPress={() => {
                                let productsStack = this.state.productsStack;
                                productsStack = productsStack.filter(function (e) {
                                    return e.index !== data.index;
                                });
                                this.setState({productsStack: productsStack});
                            }}>
                            <Icon name="close" style={{color: 'red', fontSize: 25, }}/>
                        </Button>
                    </View>
                </View>
            );
        }
    };

    addNewProduct = () =>{
        let mythis = this;
        this.setState({manualAddVisible:false}, ()=>{
            console.log('after');

            setTimeout(function () {
                mythis.props.history.push({
                    pathname: '/product-form',
                    state: {
                        newBarcode: mythis.state.barcodes[0].data,
                    },
                });
            }, 1000)

        })

    };

    render() {
        return (
            <View style={styles.container}>
                <RNCamera
                    ref={ref => {
                        this.camera = ref;
                    }}
                    style={styles.preview}
                    type={RNCamera.Constants.Type.back}
                    flashMode={RNCamera.Constants.FlashMode.off}
                    androidCameraPermissionOptions={{
                        title: 'Permission to use camera',
                        message: 'We need your permission to use your camera',
                        buttonPositive: 'Ok',
                        buttonNegative: 'Cancel',
                    }}
                    androidRecordAudioPermissionOptions={{
                        title: 'Permission to use audio recording',
                        message: 'We need your permission to use your audio',
                        buttonPositive: 'Ok',
                        buttonNegative: 'Cancel',
                    }}
                    onGoogleVisionBarcodesDetected={({barcodes}) => {
                        console.log(barcodes);
                        this.setState({
                            barcodes: barcodes,
                        }, async () => {
                            if (this.state.barcodes.length && !this.state.barcodesModalVisible) {
                                const response = await productSearchRequest(
                                    this.state.token,
                                    'simple',
                                    {
                                        search_key: 'barcode',
                                        search_value: this.state.barcodes[0].data
                                    });
                                console.log(response);
                                if (response.error !== null) {
                                    console.log('error searching');
                                    console.log(response.result);
                                    this.setState({
                                        manualAddVisible: true
                                    });
                                } else {
                                    console.log(response.result);
                                    this.setState({
                                        barcodesSearchResult: response.result[0],
                                        barcodesModalVisible: true,
                                    });
                                }
                            }
                        });
                    }}
                />
                {this.renderBarcodes()}
                <Dialog
                    width={0.7}
                    dialogTitle={<DialogTitle title="Ничего не нашлось"/>}
                    visible={this.state.manualAddVisible}
                    dialogAnimation={
                        new FadeAnimation({
                            initialValue: 0,
                            animationDuration: 150,
                            useNativeDriver: true,
                        })
                    }
                    onTouchOutside={() => {
                        console.log('touch outside');
                        this.setState({
                            manualAddVisible: false,
                        })
                    }}
                    footer={
                        <DialogFooter>
                            <DialogButton
                                text="Нет"
                                onPress={() => {
                                    this.setState({
                                        manualAddVisible: false,
                                    })
                                }}
                            />
                            <DialogButton
                                text="Да"
                                onPress={() => {
                                    this.addNewProduct();
                                }}
                            />
                        </DialogFooter>
                    }
                >
                    <DialogContent style={{marginTop: 12, marginBottom: 12, }}>
                        <Text style={{fontSize: 16, textAlign: 'center'}}>
                            Вы можете внести новый продукт вручную,
                            заполнив полное название, содержание белков, жиров и углеводов.
                        </Text>
                        <Text style={{fontSize: 16, textAlign: 'center'}}>
                            Хотите добавить новый продукт?
                        </Text>
                    </DialogContent>
                </Dialog>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.detectionsModalVisible}
                    onRequestClose={() => console.log('close')}>
                    <View
                        style={styles.modalContainer}>
                        <View
                            style={styles.modalRow}>
                            <View
                                style={{
                                    width: '100%',
                                    flexDirection: 'column',
                                    alignItems: 'center',
                                }}>
                                <Title style={{marginBottom: 20}}>Выберите продукт</Title>
                                {/*<ListView*/}
                                {/*  style={{flex: 1}}*/}
                                {/*  data={this.state.detections}*/}
                                {/*  renderRow={this.renderDetectionRow}*/}
                                {/*/>*/}
                                {this.state.detections.filter(data=>{
                                    console.log(data);
                                    console.log(detectionIndexes.includes(data.index));
                                    console.log(data.index); return detectionIndexes.includes(data.index)}).length ? (
                                  <ListView
                                    style={{flex: 1}}
                                    data={this.state.detections}
                                    renderRow={this.renderDetectionRow}
                                  />
                                ):(
                                  <Text style={{marginVertical: 10, fontSize:18, fontWeight: 'bold'}}>Продукты не обнаружены</Text>
                                )}

                            </View>
                            <Button
                                style={{
                                    position: 'absolute',
                                    top: 5,
                                    right: 0,
                                }}

                                styleName="clear"
                                onPress={() => {
                                    this.setState({detectionsModalVisible: false});
                                }}>
                                <Icon name="close" style={{color: 'red'}}/>
                            </Button>
                        </View>
                    </View>
                </Modal>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.stackModalVisible}
                    onRequestClose={() => console.log('close')}>
                    <View
                        style={styles.modalContainer}>
                        <View
                            style={styles.modalRow}>
                            <View
                                style={{
                                    width: '100%',
                                    flexDirection: 'column',
                                    alignItems: 'center',
                                }}>
                                <Title style={{marginBottom: 20}}>Добавленные продукты</Title>
                                <ListView
                                    style={{flex: 1}}
                                    data={this.state.productsStack}
                                    renderRow={this.renderProductEditRow}
                                />
                            </View>
                            <Button
                                style={{
                                    height: 50,
                                    borderRadius: 25,
                                    backgroundColor: '#FF5722',
                                }}
                                stylename={'primary'}
                                onPress={() => {
                                    console.log('add this to product form');
                                    this.addBarcodeProduct();
                                }}
                            >
                                <Text style={{color: '#ffffff'}}>Добавить все</Text>
                            </Button>
                            <Button
                                style={{
                                    position: 'absolute',
                                    top: 5,
                                    right: 0,
                                }}
                                styleName="clear"
                                onPress={() => {
                                    this.setState({stackModalVisible: false});
                                }}>
                                <Icon name="close" style={{color: 'red'}}/>
                            </Button>
                        </View>
                    </View>
                </Modal>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.barcodesModalVisible}
                    onRequestClose={() => console.log('close')}>
                    <View
                        style={styles.modalContainer__small}>
                        <View
                            style={styles.modalRow}>
                            {this.renderBarcodesModal()}

                            <View style={styles.bottomRow}>
                                <Button
                                    style={{
                                        height: 50,
                                        flexDirection: 'row',
                                        borderRadius: 15,
                                        width: 120,
                                        verticalAlign: 'center',
                                        backgroundColor: '#FF5722',
                                        alignItems: 'center',
                                        justifyContent: 'center'
                                    }}
                                    stylename={'primary'}
                                    onPress={() => {
                                        let productsStack = this.state.productsStack;
                                        let res = this.state.barcodesSearchResult;
                                        res.index = new Date().getTime();
                                        productsStack.push(res);
                                        this.setState({productsStack: productsStack}, ()=>{
                                            this.addBarcodeProduct();
                                        });
                                    }}>
                                    <Text style={{ color: '#ffffff', margin:0, textAlign: 'center' }}>Продолжить</Text>
                                </Button>
                                <Button
                                    style={{
                                        flexDirection: 'row',
                                        height: 50,
                                        borderRadius: 15,
                                        verticalAlign: 'center',
                                        backgroundColor: '#FF5722',
                                        alignItems: 'center',
                                        width: 120,
                                    }}
                                    stylename={'primary'}
                                    onPress={() => {
                                        this.addToStack();

                                    }}
                                >
                                    <Text style={{ color: '#ffffff', margin:0, textAlign: 'center' }}>Добавить еще</Text>
                                </Button>
                            </View>

                            <Button
                                style={{
                                    position: 'absolute',
                                    top: 5,
                                    right: 0,
                                }}
                                styleName="clear"
                                onPress={() => {
                                    this.setState({barcodesModalVisible: false});
                                }}>
                                <Icon name="close" style={{color: 'red'}}/>
                            </Button>
                        </View>
                    </View>
                </Modal>
                <View style={styles.captureBtnHolder}>
                    <TouchableOpacity onPress={this.takePicture.bind(this)} style={styles.capture}>
                        <Icon name='take-a-photo' style={styles.captureIcon}/>
                    </TouchableOpacity>
                </View>
                {this.renderProductsStackCounter()}
            </View>
        );
    };

    renderDetectionRow = (data) => {
        console.log('RENDER DETECTION ROW');
        console.log(data)
        if (detectionIndexes.includes(data.index)){
            return (
                <TouchableOpacity onPress={async () => {
                    //FIXME: Hardcode!!! replace to data.label
                    console.log(data);
                    console.log(data.label);
                    let response = await productSearchRequest(
                        this.state.token,
                        'simple',
                        {
                            search_key: 'name',
                            search_value: data.label,
                        });
                    if (response.result !== null) {
                        if (response.result.length !== 0) {
                            this.setState({
                                barcodesSearchResult: response.result[0]
                            }, () => {
                                // console.log(this.state.barcodesSearchResult);
                                let productsStack = this.state.productsStack;
                                let res = this.state.barcodesSearchResult;
                                res.index = new Date().getTime();
                                productsStack.push(res);
                                this.setState({productsStack: productsStack}, ()=>{
                                    this.addBarcodeProduct();
                                });
                                // this.addBarcodeProduct();
                            });
                        } else {
                            showSnackBar('Не найдено продуктов', 'LONG', 'Ок', () => {
                            });
                            console.log(response.error);
                        }
                    } else {
                        showSnackBar('Ошибка поиска', 'SHORT', 'ОК', () => {
                        });
                        console.log(response.error);
                    }
                }} style={styles.detectionRow}>
                    <Text style={{fontSize: 20}}> {data.label} </Text>
                </TouchableOpacity>
            );
        }

    };

    renderBarcodes = () => (
        <View style={styles.facesContainer} pointerEvents="none">
            {this.state.barcodes.map(this.renderBarcode)}
        </View>
    );

    renderBarcode = ({bounds, data, type}) => (
        <React.Fragment key={data + bounds.origin.x}>
            <View
                style={[
                    styles.text,
                    {
                        ...bounds.size,
                        left: bounds.origin.x,
                        top: bounds.origin.y,
                    },
                ]}
            >
                <Text style={[styles.textBlock]}>{`${data} ${type}`}</Text>
            </View>
        </React.Fragment>
    );

    takePicture = async () => {
        if (this.camera) {
            const options = {quality: 0.5, base64: true};
            const data = await this.camera.takePictureAsync(options);

            this.tflite.runModelOnImage({
                    path: data.uri,  // required
                    imageMean: 128.0, // defaults to 127.5
                    imageStd: 128.0,  // defaults to 127.5
                    numResults: 5,    // defaults to 5
                    threshold: 0.05   // defaults to 0.1
                },
                (err, res) => {
                    if (err) {
                        console.log('error detection');
                        console.log(err);
                    } else {
                        console.log('success detection');
                        console.log(res);

                        this.setState({
                            detections: res,
                            detectionsModalVisible: true,
                        });
                    }
                });
        }
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'black',
    },
    modalContainer: {
        backgroundColor: 'white',
        width: '80%',
        height: '70%',
        alignSelf: 'center',
        top: '15%',
        borderRadius: 3,
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: {width: 100, height: 100},
        shadowOpacity: 0.8,
        shadowRadius: 82,
        elevation: 1,
    },
    modalContainer__small: {
        backgroundColor: 'white',
        width: '80%',
        height: '45%',
        alignSelf: 'center',
        top: '30%',
        borderRadius: 3,
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: {width: 100, height: 100},
        shadowOpacity: 0.8,
        shadowRadius: 82,
        elevation: 1,
    },
    modalRow: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: '100%',
        height: '100%',
        padding: 25,
    },
    bottomRow: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        width: '100%',
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    productsStackCounter: {
        position: 'absolute',
        bottom: 10,
        left: 0,
        right: 20,
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    productsStackBtn: {
        flex: 0,
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: '#FF5722',
        borderRadius: 100,
        padding: 15,
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        margin: 0,
    },
    captureBtnHolder: {
        position: 'absolute',
        bottom: 10,
        left: 0,
        right: 0,
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    capture: {
        flex: 0,
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: '#fff',
        borderRadius: 100,
        padding: 15,
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        margin: 0,
    },
    captureIcon: {
        alignItems: 'center',
        justifyContent: 'center',

    },
    facesContainer: {
        position: 'absolute',
        bottom: 0,
        right: 0,
        left: 0,
        top: 0,
    },
    detectionRow: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        // marginTop: 15,
        paddingTop: 15,
        paddingBottom: 15,
        // marginBottom: 15,
        width: '100%'
    },
    section__se: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: 'white',
        marginBottom: 20,
        marginTop: 20,
        flexWrap: 'wrap',
        paddingVertical: 10,
    },
    col: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
    },
    key: {
        fontSize: 12,
    },
    value: {
        fontSize: 20,
        fontWeight: 'bold',
        color: '#FF5722',
    },
    btnRow: {
        display: 'flex',
        flexDirection: 'row',
        position: 'absolute',
        width: '10%',
        backgroundColor: 'black',
        color: 'black',
        alignItems: 'flex-start',
        justifyContent: 'space-between'
    },
});

const mapStateToProps = state => ({
    token: state.authentication.token,
});

export default connect(mapStateToProps)(Camera);
