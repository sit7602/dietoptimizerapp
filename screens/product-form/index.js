import React from 'react';
import {
  AsyncStorage,
  View,
ScrollView,
  BackHandler,
  StyleSheet,
  Modal,
  ToastAndroid,
  Image,
  SafeAreaView,
  ActivityIndicator,
  Picker,
} from 'react-native';

import {NavigationBar, Button, Icon, Title, Text, TextInput, ListView, Screen} from '@shoutem/ui';
import moment from "moment";
// import Slider from '@react-native-community/slider';
import {
  Autocomplete,
  withKeyboardAwareScrollView,
} from 'react-native-dropdown-autocomplete';
import FAB from 'react-native-fab';
import {Dropdown} from 'react-native-material-dropdown';
import {connect} from 'react-redux';
import mealsRequest from '../../network/mealsRequest';
import showSnackBar from '../../components/showSnackbar';
import LoadingButton from '../authentication/login/LoadingButton';
import NativeToastAndroid from "react-native/Libraries/Components/ToastAndroid/NativeToastAndroid";

class ProductForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      ccpf:[0,0,0,0],
      isLoading: 0,
      token: '',
      modalVisible: false,
      calorage: 0,
      selectedMeal: '',
      selectedProduct: null,
      unitsInPortion: 1,
      numberOfPortions: 1,
      newMeal: '',
      userMeals: [],
      products: [],
      autocompleteValue: '',
      btnLoading: false,
      productList: [],
      productModalVisible: false,
      foundCal: 0,
      foundFat: 0,
      foundCarb: 0,
      foundProtein: 0,
      foundName: '',
      foundUnitsPerPortion: 0,
      newBarcode: undefined,
      // vanya dialog
      manualAddVisible: false,
      addProductDisabled: true,
    };
    this.host = 'http://tech.splinex-team.com:6667';
    this.product_search_url = `${this.host}/api/v1/user/product/search`;
    //`${this.product_search_url}/?name="${this.state.autocompleteValue}"`
  }

  componentWillMount() {
    const token = `Bearer_${this.props.token}`;
    if (this.props.location.state && this.props.location.state.productList){
        console.log('Product form MOUNT');
        console.log(this.props.location.state.productList);

      const productList = this.props.location.state.productList;
      const cal = productList
        .map(e=> {
          const calPerOneGramm = e.selectedProduct.calPer100Units/100;
          const calories = e.unitsInPortion;
          const prCal = Math.round(calPerOneGramm * calories);
          console.log(prCal);
          return prCal
        })
        .reduce((sum,e)=>sum+e);
      const prot = productList.map(e=> e.selectedProduct.protein || 0).reduce((sum,e)=>sum+e);
      const carb = productList.map(e=> e.selectedProduct.carb || 0).reduce((sum,e)=>sum+e);
      const fat = productList.map(e=> e.selectedProduct.fat || 0).reduce((sum,e)=>sum+e);
      this.setState({
        productList: this.props.location.state.productList,
        ccpf:[cal, carb, prot, fat]
      });



    }
    if (this.props.location.state && this.props.location.state.newBarcode) {
      this.setState({
        newBarcode: this.props.location.state.newBarcode,
        productModalVisible: true
      });
    }
    this.setState(
      {
        token: token,
      },
      async () => {
        const response = await mealsRequest(this.state.token);
        // console.log('MEALS REQUEST');
        // console.log(response.result);
        if (response.result !== null && response.result.length) {
          let userMeals = [];
          response.result.forEach((el, index) => {
            // console.log(moment(el.created).format('DD MMM'),moment(el.created).diff(moment(), 'days'));
            if (moment(el.created).isSame(moment(), 'day')) {
              userMeals.push({
                id: el.id !== undefined ? el.id : index,
                value: el.name,
              });
            }
          });
          console.log(userMeals);
          const selectedMeal = userMeals.length ? {id:userMeals[0].id, value: userMeals[0].value} : '';

          this.setState({userMeals: userMeals, selectedMeal: selectedMeal});
        }
        this.setState({
          isLoading: this.state.isLoading + 1,
        });
      },
    );
  }

  addEatenProduct = () => {
    let mealId;
    console.log('this.state.selectedMeal');
    console.log(this.state.selectedMeal);
    if (this.state.productList.length > 0 && this.state.selectedMeal && this.state.selectedMeal.id) {
      mealId = this.state.selectedMeal.id;
      this.setState({btnLoading: true});
      const products = this.state.productList.map(p=>{
        const {
          unitsInPortion,
          numberOfPortions,
        } = p;

        const newProduct = {
          unitsInPortion,
          numberOfPortions,
          product_id: p.selectedProduct.id
        };
        return newProduct
      });
      console.log(products);
      let headers = {
        'Content-Type': 'application/json',
        Authorization: this.state.token,
      };
      // return;
      fetch(
        `${this.host}/api/v1/user/meal/${parseInt(mealId)}/eaten-product/add-multiple`,
        {
          method: 'POST',
          headers: headers,
          body: JSON.stringify(products),
        },
      )
        .then(response => response.json())
        .then(resp => {
          if (resp.eaten_products.length) {
            this.setState({btnLoading: false}, () => {
              this.props.history.push({
                pathname: '/main',
              });
              // this.props.history.goBack();
            });
          } else {
            showSnackBar('Ошибка добавления продукта', 'SHORT', 'ОК', () => {});
          }
        })
        .catch(err => {
          console.log(err);
          showSnackBar('Ошибка добавления продукта', 'SHORT', 'ОК', () => {});
        });
    } else {
      console.log(this.state.selectedMeal);
      if (this.state.selectedMeal === '') {
        showSnackBar('Выберите прием пищи', 'SHORT', 'ОК', () => {});
      } else {
        showSnackBar('Добавьте продукты', 'SHORT', 'ОК', () => {});
      }
    }

  };

  renderProduct = (product) => {
    console.log('render product');
    console.log(product);
    let calPerOneGramm = 0;
    if (product.selectedProduct.calPer100Units) {
      calPerOneGramm = product.selectedProduct.calPer100Units/100;
    }
    const grams = product.unitsInPortion;
    const prCal = Math.round(calPerOneGramm * grams);

    let product_name =
      product.selectedProduct.name.length > 18
        ? `${product.selectedProduct.name.substring(0, 18)}...`
        : product.selectedProduct.name;
    return (
      <View style={[styles.productRow, {backgroundColor:'white'}]}>
        <View style={styles.caloriesTextHolder}>
          <Text style={styles.caloriesText}>{product_name}</Text>
        </View>
        <View style={styles.caloriesHolder}>
          <Text>Калории: {prCal} X {product.numberOfPortions} </Text>
        </View>
      </View>
    );
  };

  componentWillUnmount() {
    this.removeAndroidBackButtonHandler();
  }

  componentDidMount() {
    this.handleAndroidBackButton();
  }

  handleAndroidBackButton = () => {
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.history.goBack();
      return true;
    });
  };

  removeAndroidBackButtonHandler = () => {
    BackHandler.removeEventListener('hardwareBackPress', () => {});
  };

  handleSelectItem(item, index) {
    // this.setState({foundName: item.name, addProductDisabled: false,});
    const unitsInPortion = item.unitsPerPortion || 0;
    let foundCal =  0;
    if (item.calPer100Units) {
      foundCal = item.calPer100Units / 100 * unitsInPortion;
    }
    let foundFat = 0;
    if (item.fat) {
      foundFat = item.fat;
    }
    let foundCarb = 0;
    if (item.carb) {
      foundCarb = item.carb;
    }
    let foundProtein = 0;
    if (item.protein) {
      foundProtein = item.protein;
    }

    this.setState({
      selectedProduct: item,
      unitsInPortion: unitsInPortion,
      addProductDisabled: false,
      foundName: item.name,
      foundCal,
      foundFat,
      foundCarb,
      foundProtein
    });
  }

  onSelect = selectedOption => {
    this.setState({selectedMeal: selectedOption});
  };

  createNewMeal = () => {
    this.toggleModalVisibility();
  };

  toggleModalVisibility = () => {
    this.setState({modalVisible: !this.state.modalVisible});
  };

  toggleAddProductModal = () => {
    this.setState({
      productModalVisible: !this.state.productModalVisible,
      unitsInPortion: 0,
      foundCal: 0,
      foundFat: 0,
      foundCarb: 0,
      foundProtein: 0
    });
  };

  saveNewMeal = () => {
    let userMeals = this.state.userMeals;
    let headers = {
      'Content-Type': 'application/json',
      Authorization: this.state.token,
    };
    let newMeal = this.state.newMeal;
    if (newMeal.trim() === '') {
      ToastAndroid.show('Please enter correct name', ToastAndroid.SHORT);
      return;
    }
    fetch(this.host + '/api/v1/user/meal/add', {
      method: 'POST',
      mode: 'no-cors',
      cache: 'no-cache',
      redirect: 'manual',
      headers: headers,
      body: JSON.stringify({
        name: this.state.newMeal,
      }),
    })
      .then(response => response.json())
      .then(resp => {
        userMeals.push({
          id: resp.id,
          value: resp.name,
        });
        this.setState({
          userMeals: userMeals,
          selectedMeal: userMeals[userMeals.length - 1].value,
        });
        this.setState({newMeal: ''});
        this.toggleModalVisibility();
      })
      .catch(error => {});
  };

  addProduct = () => {

    const {
      productList,
      unitsInPortion,
      numberOfPortions,
      selectedProduct,
      addProductDisabled,
    } = this.state;
    // console.log(selectedProduct);
    if (!addProductDisabled) {
      if (!selectedProduct) {
        console.log('IFF addProduct');
        NativeToastAndroid.show('Добавьте продукты', NativeToastAndroid.SHORT);
        // showSnackBar('Добавьте продукты', 'SHORT', 'ОК', () => {});
      } else {
        const product = {
          unitsInPortion,
          numberOfPortions,
          selectedProduct
        };
        productList.push(product);
        const cal = productList
          .map(e=> {
            const calPerOneGramm = e.selectedProduct.calPer100Units/100;
            const calories = e.unitsInPortion;
            const prCal = Math.round(calPerOneGramm * calories);
            console.log(prCal);
            return prCal
          })
          .reduce((sum,e)=>sum+e);
        const prot = productList.map(e=> e.selectedProduct.protein || 0).reduce((sum,e)=>sum+e);
        const carb = productList.map(e=> e.selectedProduct.carb || 0).reduce((sum,e)=>sum+e);
        const fat = productList.map(e=> e.selectedProduct.fat || 0).reduce((sum,e)=>sum+e);
        const ar1 = JSON.parse(JSON.stringify(productList));
        this.setState({productList: ar1, productModalVisible: false, ccpf:[cal, carb, prot, fat]});
      }
    } else {
      NativeToastAndroid.show('Выберите продукт', NativeToastAndroid.SHORT);
    }

  };

  saveProduct = () => {
    const {
      productList,
      unitsInPortion,
      numberOfPortions,
      selectedProduct
    } = this.state;
    console.log(selectedProduct);
    let data;
    if (!this.state.foundName.length) {
      NativeToastAndroid.show('Введите название продукта', NativeToastAndroid.SHORT);
      // showSnackBar('Введите название продукта', 'SHORT', 'ОК', () => {});
      return false;
    }
    if (!selectedProduct || selectedProduct && selectedProduct.name != this.state.foundName) {
      console.log('IF not selected product');
      // showSnackBar('Ошибка добавления продукта', 'SHORT', 'ОК', () => {});
      // return false;
      // showSnackBar('Введите название нового продукта', 'SHORT', 'OK', ()=>{});
      data = {
        name: this.state.foundName,
        brand: 'user added',
        type: 'user added',
        categoryId: 6,
        units: 'grams',
        calPer100Units: Math.round(this.state.foundCal) / parseInt(this.state.unitsInPortion) * 100,
        unitsPerPortion: parseInt(this.state.unitsInPortion),
        protein: this.state.foundProtein,
        fat: this.state.foundFat,
        carb: this.state.foundCarb,
      };

      if (this.state.newBarcode !== undefined) {
        data.barcode = this.state.newBarcode
      }

    } else {
      console.log('IF YES selected product');
      data = {
        name: this.state.selectedProduct.name,
        brand: 'user added',
        type: 'user added',
        categoryId: 6,
        units: 'grams',
        calPer100Units: this.state.selectedProduct.calPer100Units,
        unitsPerPortion: this.state.selectedProduct.unitsPerPortion,
        protein: this.state.selectedProduct.protein,
        fat: this.state.selectedProduct.fat,
        carb: this.state.selectedProduct.carb,
      };
    }
    let headers = {
      'Content-Type': 'application/json',
      Authorization: this.state.token,
    };
    console.log(data);

    fetch(
      `${this.host}/api/v1/user/product/add`,
      {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(data),
      },
    )
      .then(response => response.json())
      .then(resp => {
        console.log(resp);
        console.log('Product add success');
        this.setState({selectedProduct: resp, addProductDisabled: false});
        this.resetNewBarcode();
        NativeToastAndroid.show('Продукт добавлен', NativeToastAndroid.SHORT);
        // showSnackBar('Продукт добавлен', 500, 'Продукт добавлен', 'OK')
      })
      .catch(err => {
        console.log(err);
        NativeToastAndroid.show('Ошибка добавления продукта', NativeToastAndroid.SHORT);
        // showSnackBar('Ошибка добавления продукта', 'SHORT', 'ОК', () => {});
      });
  };

  resetNewBarcode = () => {
    this.setState({newBarcode: undefined})
  };

  renderProductList = () =>{
    if (this.state.productList.length === 0) {
      return (
        <Text style={{marginVertical: 10, fontSize:20, fontWeight: 'bold'}}>Продукты не внесены</Text>
      )
    } else {
      return (
        <ListView
          data={this.state.productList}
          renderRow={this.renderProduct}
        />
      )
    }
  };

  fetchProductsSearch(product_name) {
    this.setState({foundName: product_name});
    let url =
      product_name !== undefined
        ? `${this.product_search_url}?q=name:${product_name}`
        : `${this.product_search_url}?search`;
    let headers = {
      'Content-Type': 'application/json',
      Authorization: this.state.token,
    };
    console.log('REQUEST');
    return fetch(url, {
      method: 'GET',
      headers: headers,
      mode: 'no-cors',
      cache: 'no-cache',
      redirect: 'manual',
    })
      .then(response => response.json())
      .then(resp => {
        console.log("RESPONSE");
        console.log(resp);
        this.setState({
          products: resp,
        });

        return resp;
      })
      .catch(error => {
        return [];
      });
  };

  render() {
    // const addProductStyle = !this.state.addProductDisabled ? styles.btnDefault : styles.btnDisabled;

    // console.log(addProductStyle);
    if (this.state.newBarcode !== undefined && !this.state.productModalVisible) {
      this.resetNewBarcode();
    }
    if (this.state.isLoading !== 1) {
      return (
        <Screen style={{flex: 1, flexDirection: 'column', justifyContent: 'center'}}>
          <View>
            <ActivityIndicator size={50}/>
          </View>
        </Screen>
      );
    }
    const defaultMeal = this.state.userMeals.length ? this.state.userMeals[0].value : 'Создайте прием пищи';
    let {foundCal, foundFat, foundProtein, foundCarb} = this.state;
    foundCal = Math.round(foundCal).toString();
    foundFat = Math.round(foundFat).toString();
    foundProtein = Math.round(foundProtein).toString();
    foundCarb = Math.round(foundCarb).toString();

    // console.log('render');
    // console.log(foundCal, foundFat, foundProtein, foundCarb);
    console.log('init = ', this.state.unitsInPortion)
    return (
      <View style={{flex: 1, paddingTop: 50}}>
        <Button
          style={{
            position: 'absolute',
            top:5,
            left:-10,
            color: 'black',
            fontSize: 40
          }}
          styleName="clear"
          onPress={()=>{this.props.history.goBack()}}
        >
          <Icon name="left-arrow" style={{color: 'black', fontSize: 40}} />
        </Button>
        <Modal
          style={{paddingTop: 50, }}
          animationType="slide"
          visible={this.state.productModalVisible}
          transparent={true}
        >
          <View
            style={{
              backgroundColor: 'white',
              width: '100%',
              height: '100%',
              alignSelf: 'center',
              top: 0,
              alignItems: 'center',
            }}>
            <View style={[styles.section111,{marginTop: 40}]}>
              <SafeAreaView style={{width: '100%', zIndex: 3}}>
                <Autocomplete
                  style={{zIndex: 2}}
                  inputStyle={{
                    width: '100%',
                    borderColor: 'rgba(0,0,0,0.2)',
                    paddingBottom: 8,
                    alignItems: 'center',
                  }}
                  initialValue={this.state.selectedProduct === null ? '' : this.state.selectedProduct.name}
                  key={item => item.id}
                  placeholder={'Найти продукт'}
                  scrollStyle={{}}
                  pickerStyle={{zIndex: 2, borderBottomColor: 'transparent',width:'90%',marginLeft:'5%'}}
                  containerStyle={{zIndex: 2}}
                  valueExtractor={item => item.name}
                  fetchData={val => this.fetchProductsSearch(val)}
                  handleSelectItem={(item, id) => this.handleSelectItem(item, id)}
                  dropdownMargins={{min:0, max:0}}
                  dropdownOffset={{top: 0, left: 0}}
                  renderIcon={() => (
                    <Icon
                      style={{
                        position: 'absolute',
                        left: 9,
                        marginVertical: 0,
                        top: 0,
                        bottom: 0,
                      }}
                      name="search"
                    />
                  )}
                  minimumCharactersCount={2}

                  highlightText
                />
              </SafeAreaView>
            </View>
            <View style={styles.section}>
              <Text style={styles.section__title}>Грамм в порции</Text>
              <View style={styles.section}>
                <TextInput
                    keyboardType={'numeric'}
                    value={''+this.state.unitsInPortion}
                    style={styles.textInput}

                    onChangeText={(val) => {
                      console.log('val = ', val);
                      let value = val.length && parseInt(val) > 0 ? ''+parseInt(val) : 0;
                      console.log('value = ', value);
                      this.setState({
                        unitsInPortion: value
                      })}
                    }
                />
                {/*<Button*/}
                {/*  style={styles.fab1}*/}
                {/*  onPress={() => {*/}
                {/*    const currentVal = this.state.unitsInPortion;*/}
                {/*    if (currentVal !== 1) {*/}
                {/*      this.setState({unitsInPortion: currentVal - 1});*/}
                {/*    }*/}
                {/*  }}>*/}
                {/*  <Icon style={styles.fab1__icon} name="minus-button" />*/}
                {/*</Button>*/}
                {/*<Text style={styles.between__text}>*/}
                {/*  {this.state.unitsInPortion}*/}
                {/*</Text>*/}
                {/*<Button*/}
                {/*  style={styles.fab1}*/}
                {/*  onPress={() => {*/}
                {/*    const currentVal = this.state.unitsInPortion;*/}
                {/*    this.setState({unitsInPortion: currentVal + 1});*/}
                {/*  }}>*/}
                {/*  <Icon style={styles.fab1__icon} name="plus-button" />*/}
                {/*</Button>*/}
              </View>
            </View>
            <View style={styles.section}>
              <Text style={styles.section__title}>Кол-во порций</Text>
              <View style={styles.section}>
                <Button
                  style={styles.fab1}
                  onPress={() => {
                    const currentVal = this.state.numberOfPortions;
                    if (currentVal !== 1) {
                      this.setState({numberOfPortions: currentVal - 1});
                    }
                  }}>
                  <Icon style={styles.fab1__icon} name="minus-button" />
                </Button>
                <Text style={styles.between__text}>
                  {this.state.numberOfPortions}
                </Text>
                <Button
                  style={styles.fab1}
                  onPress={() => {
                    const currentVal = this.state.numberOfPortions;
                    this.setState({numberOfPortions: currentVal + 1});
                  }}>
                  <Icon style={styles.fab1__icon} name="plus-button" />
                </Button>
              </View>
            </View>
            <View style={styles.section__se}>
              <View style={styles.col}>
                <TextInput keyboardType={'numeric'}
                           onChangeText={(val)=>{
                             const value = val.length ? parseInt(val) : 0;
                             if (this.state.selectedProduct) {
                                 let selectedProduct = this.state.selectedProduct;
                                 selectedProduct['calPer100Units'] = value/ selectedProduct['unitsPerPortion'] * 100
                                 this.setState(selectedProduct)
                               }
                               this.setState({foundCal: value})
                             }
                           }
                           value={foundCal}
                           style={styles.input1}
                           placeholder={foundCal}
                />
                <Text style={styles.key}>калории</Text>
              </View>
              <View style={styles.col}>
                <TextInput keyboardType={'numeric'}
                           onChangeText={(val)=>{
                             const value = val.length ? parseInt(val) : 0;
                             if (this.state.selectedProduct) {
                               let selectedProduct = this.state.selectedProduct;
                               selectedProduct['protein'] = value;
                               this.setState(selectedProduct)
                             }
                             this.setState({foundProtein: value})
                             }
                           }
                           value={foundProtein}
                           style={styles.input1}
                           placeholder={foundProtein}
                />
                <Text style={styles.key}>белки</Text>
              </View>
              <View style={styles.col}>
                <TextInput keyboardType={'numeric'}
                           onChangeText={(val)=>{
                             const value = val.length ? parseInt(val) : 0;
                             if (this.state.selectedProduct) {
                               console.log(val);
                               let selectedProduct = this.state.selectedProduct;
                               selectedProduct['fat'] = value;
                               this.setState(selectedProduct)
                             }
                             this.setState({foundFat: value})
                             }
                           }

                           value={foundFat}
                           style={styles.input1}
                           placeholder={foundFat}
                />
                <Text style={styles.key}>жиры</Text>
              </View>
              <View style={styles.col}>
                <TextInput keyboardType={'numeric'}
                           onChangeText={(val)=>{
                             const value = val.length ? parseInt(val) : 0;
                             if (this.state.selectedProduct) {
                               let selectedProduct = this.state.selectedProduct;
                               selectedProduct['carb'] = value;
                               this.setState(selectedProduct)
                             }
                             this.setState({foundCarb: value})
                           }
                           }
                           value={foundCarb}
                           style={styles.input1}
                           placeholder={foundCarb}
                />
                <Text style={styles.key}>углеводы</Text>
              </View>
            </View>
            <View style={styles.section__se}>
              <View style={styles.col}>
                <TextInput keyboardType={'numeric'}
                           onChangeText={(val)=> {
                             const value = val.length ? val : '';
                             this.setState({newBarcode: value})

                            }
                           }
                           value={this.state.newBarcode}
                           style={styles.input1}
                           placeholder={this.state.newBarcode || ''}
                />
                <Text style={styles.key}>штрихкод</Text>
              </View>
            </View>
            <View style={styles.section__sb}>
              <Button
                ref={this.btnAddProduct = React.createRef()}
                style={this.state.addProductDisabled ? styles.btnDisabled : styles.btnDefault}
                stylename={'primary'}
                disabled={this.state.addProductDisabled}
                onPress={() => this.addProduct()}>
                <Text style={{color: '#ffffff'}}>{ this.state.addProductDisabled ? 'Добавить продукт' : 'Добавить продукт'}</Text>
              </Button>
              <Button
                style={{
                  height: 50,
                  borderRadius: 25,
                  backgroundColor: '#FF5722',
                }}
                stylename={'primary'}
                onPress={() => this.saveProduct()}>
                <Text style={{color: '#ffffff'}}>Сохранить</Text>
              </Button>
            </View>

            <Button
              style={{
                position: 'absolute',
                top:5,
                left:-10,
                color: 'black',
                fontSize: 40
              }}
              styleName="clear"
              onPress={() => {
                this.resetNewBarcode();
                this.toggleAddProductModal();
              }}>
              <Icon name="left-arrow" style={{color: 'black', fontSize: 40}} />
            </Button>
          </View>
        </Modal>
        <View style={styles.container}>
          <NavigationBar
            leftComponent={
              <Button
                onPress={() => {
                  this.props.history.goBack();
                }}>
                <Icon name="back" />
              </Button>
            }
            centerComponent={<Title>Add new product</Title>}
          />
          <Modal
            style={{}}
            animationType="slide"
            transparent={true}
            visible={this.state.modalVisible}>
            <View
              style={{
                backgroundColor: 'white',
                width: '80%',
                height: '70%',
                alignSelf: 'center',
                top: '15%',
                borderRadius: 3,
                alignItems: 'center',
                borderWidth: 1,
                borderColor: '#ddd',
                borderBottomWidth: 0,
                shadowColor: '#000',
                shadowOffset: {width: 100, height: 100},
                shadowOpacity: 0.8,
                shadowRadius: 82,
                elevation: 1,
              }}>
              <View
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  width: '100%',
                  height: '100%',
                  padding: 20,
                }}>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'column',
                    alignItems: 'center',
                  }}>
                  <Title style={{marginBottom: 20}}>Создать новый прием пищи</Title>
                  <TextInput
                    style={{
                      borderColor: 'rgba(0,0,0,0.1)',
                      borderRadius: 10,
                      borderWidth: 2,
                    }}
                    placeholder={'Введите название'}
                    onChangeText={val => this.setState({newMeal: val})}
                  />
                </View>
                <Button
                  style={{
                    height: 50,
                    borderRadius: 25,
                    backgroundColor: '#FF5722',
                  }}
                  stylename={'primary'}
                  onPress={() => this.saveNewMeal()}>
                  <Text style={{color: '#ffffff'}}>Сохранить</Text>
                </Button>
                <Button
                  style={{
                    position: 'absolute',
                    top: 5,
                    right: 0,
                  }}
                  styleName="clear"
                  onPress={() => {
                    this.toggleModalVisibility();
                  }}>
                  <Icon name="close" style={{color: 'red'}} />
                </Button>
              </View>
            </View>
          </Modal>

          <View style={styles.section__sb}>
            <Dropdown
              label="Выберите прием пищи"

              dropdownOffset={{top: 0}}
              data={this.state.userMeals}
              valueExtractor={item => item.value}
              containerStyle={{width: '65%'}}
              onChangeText={(val, index) => {
                this.setState({
                  selectedMeal: this.state.userMeals[index],
                });
              }}
              value={defaultMeal}
            />
            <Button
              style={{
                height: 50,
                borderRadius: 25,
                backgroundColor: '#FF5722',
                // right: 0,
              }}
              onPress={this.createNewMeal}>
              <Text
                style={{
                  fontSize: 12,
                  textTransform: 'uppercase',
                  color: '#ffffff',
                }}>
                Создать
              </Text>
              {/*<Icon name="plus-button" style={{color: '#fff'}} />*/}
            </Button>
          </View>
          <View style={[styles.section,{}]}>
            {this.renderProductList()}

          </View>


          <View
            style={styles.section__se1}
          >
            <Button
              style={{
                height: 50,
                borderRadius: 25,
                backgroundColor: '#FF5722',
                right: 0,
              }}
              onPress={() => {this.toggleAddProductModal()}}>
              <Text
                style={{
                  fontSize: 14,
                  textTransform: 'uppercase',
                  color: '#ffffff',
                }}>
                Добавить продукт
              </Text>
              {/*<Icon name="plus-button" style={{color: '#fff'}} />*/}
            </Button>
          </View>
          <View style={styles.section__se}>
            <View style={styles.col}>
              <Text style={styles.value}>{this.state.ccpf[0]}</Text>
              <Text style={styles.key}>калории</Text>
            </View>
            <View style={styles.col}>
              <Text style={styles.value}>{this.state.ccpf[2]}</Text>
              <Text style={styles.key}>белки</Text>
            </View>
            <View style={styles.col}>
              <Text style={styles.value}>{this.state.ccpf[3]}</Text>
              <Text style={styles.key}>жиры</Text>
            </View>
            <View style={styles.col}>
              <Text style={styles.value}>{this.state.ccpf[1]}</Text>
              <Text style={styles.key}>углеводы</Text>
            </View>
          </View>
          <View
            style={styles.section__se1}
          >
            <Button
              style={{
                height: 50,
                borderRadius: 25,
                backgroundColor: '#FF5722',
                right: 0,
              }}
              onPress={this.addEatenProduct}>
              <Text
                style={{
                  fontSize: 14,
                  textTransform: 'uppercase',
                  color: '#ffffff',
                }}>
                Сохранить
              </Text>
            </Button>
            {/*<LoadingButton*/}
            {/*  buttonText="Save"*/}
            {/*  onPress={this.addEatenProduct}*/}
            {/*  isLoading={this.state.btnLoading}*/}
            {/*  size="small"*/}
            {/*/>*/}
          </View>
        </View>
      </View>
    );
  };
}

const mapStateToProps = state => ({
  token: state.authentication.token,
});

export default connect(mapStateToProps)(ProductForm);

const styles = StyleSheet.create({
  input1: {width:'100%', marginBottom: 4, borderColor: 'black', borderWidth: 1, textAlign: 'center'},
  textInput: {
    width: '55%',
    marginBottom: 4,
    borderColor: 'black',
    borderWidth: 1,
    textAlign: 'center',
    borderRadius: 10,
  },
  caloriesText: {
    fontSize: 16,
  },
  productRow: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    paddingHorizontal: 15,
    marginVertical: 7,
  },
  caloriesTextHolder: {
    paddingTop: 5,
    display: 'flex',
  },
  section__title: {
    fontSize: 18,
    marginTop: 20,
    marginBottom: 10,
  },
  key: {
    fontSize: 12,
  },
  value: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#FF5722',
  },
  col: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  between__text: {
    marginLeft: 20,
    marginRight: 20,
    fontSize: 34,
    fontWeight: 'bold',
  },
  fab1__icon: {
    color: '#fff',
  },
  fab1: {
    width: 50,
    height: 50,
    borderRadius: 25,
    backgroundColor: '#FF5722',
    color: '#fff',
  },

  container: {
    flex:1,
    backgroundColor: '#f2f2f2',
    paddingTop: 10,
  },
  section: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    marginBottom: 20,
    flexWrap: 'wrap',
  },
  section__sb: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: 'white',
    marginBottom: 20,
    paddingTop: 10,
    paddingBottom: 10,
    // verticalAlign: 'middle',
    flexWrap: 'nowrap',
    // paddingHorizontal: 20,
    paddingVertical: 8,
  },
  section__se: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: 'white',
    marginBottom: 20,
    flexWrap: 'wrap',
    paddingVertical: 10,
  },
  section__se1: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginBottom: 20,
    flexWrap: 'wrap',
    paddingVertical: 10,
  },

  section11: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    marginBottom: 20,
    paddingVertical: 10,
    paddingHorizontal: 10,
    zIndex: 1,
  },
  section111: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    marginBottom: 20,
    paddingVertical: 10,
    paddingHorizontal: 10,
    zIndex: 2,
  },

  section1: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    marginBottom: 20,
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  input: {
    flex: 1,
    flexGrow: 1,
    marginTop: 5,
    marginBottom: 5,
    borderRadius: 30,
    color: 'white',
    width: 400,
  },

  containerSelect: {
    flexDirection: 'column',
    marginTop: 20,
    marginBottom: 20,
  },
  modalContainer: {
    width: 300,
    height: 400,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'column',
    padding: 10,
  },
  topNav: {},
  inputRow: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    width: '80%',
  },
  productForm: {
    flex: 0.2,
    // flexDirection: 'column',
    // marginTop: 10,
    alignItems: 'center',
    // backgroundColor: 'gray'
    // justifyContent: 'space-around's
  },
  autocompleteContainer: {
    paddingTop: 0,
    zIndex: 1,
    // height: '15%',
    width: '100%',
    paddingHorizontal: 8,
    // display: 'flex',
    // flex: .05,
    flexDirection: 'row',
    backgroundColor: 'green',
    // marginLeft: 0,
    // marginRight: 0,
    // justifyContent: 'center',

    // backgroundColor: 'gray'
  },
  // input: {
  //     maxHeight: 40,
  //     marginTop: 10,
  //     marginBottom: 10,
  // },
  inputAutocomplete: {
    // display: 'flex',
    width: '100%',
    // flex: 1,
    // flexDirection: 'row',
    // alignItems: 'center',
    // width: '100%',
  },

  inputContainer: {
    display: 'flex',
    flexShrink: 0,
    flexGrow: 0,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#c7c6c1',
    paddingVertical: 13,
    paddingLeft: 12,
    paddingRight: '5%',
    width: '100%',
    backgroundColor: 'gray',

    justifyContent: 'flex-start',
  },
  plus: {
    position: 'absolute',
    left: 15,
    top: 10,
    width: 20,
    height: 20,
  },
  addMealBtn: {
    width: 70,
    height: 40,
    color: 'red',
  },
  selectContainer: {
    flex: 0.4,
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'center',
    height: 20,
    alignItems: 'center',
  },
  select: {
    flex: 1,
    marginHorizontal: 4,
    width: '60%',
  },
  btnDefault: {
    opacity: 1,
    height: 50,
    borderRadius: 25,
    backgroundColor: '#FF5722',
  },
  btnDisabled: {
    opacity: .5,
    height: 50,
    borderRadius: 25,
    backgroundColor: '#FF5722',
  },
});
