import React, {useState, useEffect} from 'react';
import ProfilePageView from './ProfilePageView';
import getUserInfoRequest from '../../network/getUserInfoRequest';
import weightRequest from '../../network/weightRequest';

import {useSelector} from 'react-redux';
import showSnackBar from '../../components/showSnackbar';

const ProfilePage = () => {

  const [userInfo, setUserInfo] = useState(null);
  const [weightList, setWeightList] = useState(null);
  const [dialogInputValue, setDialogInputValue] = useState(null);
  const [isVisibleDiaolog, setVisibleDiaolog] = useState(false);
  const token = useSelector(state => state.authentication.token);
  // let loading = true;
  const [ loading, setLoading ] = useState(true);
  const graphData = {
    labels:
      weightList && weightList.length
        ? weightList.map((item, index) => index)
        : [0, 0, 0, 0, 0, 0],
    datasets: [
      {
        data:
          weightList && weightList.length
            ? weightList.map(item => item.value)
            : [0, 0, 0, 0, 0, 0],
        color: (opacity = 1) => `rgba(255, 87, 34, ${opacity})`, // optional
        strokeWidth: 1, // optional
      },
    ],
  };
  const {age, email, firstName, goalWeight, lastName, username} = userInfo
    ? userInfo
    : {
        age: 0,
        email: 0,
        firstName: 'firstName',
        goalWeight: 0,
        lastName: 'lastName',
        username: 'username',
      };
  // console.log(!!weightList);
  const currentWeight =
    weightList && weightList.length
      ? weightList[weightList.length - 1].value
      : 0;

  const lastWeightNumber =
    weightList && weightList.length ? weightList[weightList.length - 1].id : 0;

  const makeUserInfoRequest = async () => {
    const {result, error} = await getUserInfoRequest(token);
    if (!error) {
      setUserInfo(result);
      setLoading(false);
    }
    else {
      showSnackBar(error.message, 'SHORT', 'ОК', () => {});
    }
  };

  const makeWeightRequest = async () => {
    const {result, error} = await weightRequest('GET', 'get-all', token);
    if (!error) {
      setWeightList(result);
    } else {
      showSnackBar(error.message, 'SHORT', 'ОК', () => {});
    }
  };

  const addWeightRequest = async () => {
    console.log(dialogInputValue);
    console.log(token);
    await weightRequest('POST', 'add', token, dialogInputValue);
    makeWeightRequest();
  };

  const deleteWeightRequest = async () => {
    await weightRequest(
      'POST',
      `${lastWeightNumber}/delete`,
      token,
      dialogInputValue,
    );
    makeWeightRequest();
  };
  useEffect(() => {
    // console.log('useEffect')
    makeUserInfoRequest();
    makeWeightRequest();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <ProfilePageView
      name={firstName}
      weight={currentWeight}
      goalWeight={goalWeight}
      graphData={graphData}
      onWeightChange={addWeightRequest}
      isVisibleDiaolog={isVisibleDiaolog}
      setVisibleDiaolog={setVisibleDiaolog}
      dialogInputValue={dialogInputValue}
      setDialogInputValue={setDialogInputValue}
      onDialogButtonPress={addWeightRequest}
      onDeleteButtonPress={deleteWeightRequest}
      token={token}
      userInfo={userInfo}
      loading={loading}
    />
  );
};

export default ProfilePage;
