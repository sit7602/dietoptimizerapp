import React from 'react';
import {StyleSheet} from 'react-native';
import Dialog, {
  FadeAnimation,
  DialogContent,
  DialogTitle,
} from 'react-native-popup-dialog';
import {Button, Text, TextInput} from '@shoutem/ui';
import showSnackBar from '../../../components/showSnackbar';
import updateUserInfoRequest from '../../../network/updateUserInfoRequest';
import {useSelector} from 'react-redux';

const styles = StyleSheet.create({
  button: {
    backgroundColor: '#FF5722',
    borderRadius: 30,
    height: 40,
    marginTop: 10,
  },
  buttonText: {color: '#fff'},
});
const ChangeDialog = props => {
  const {
    id,
    bodyField,
    dialogTitle,
    dialogInputValue,
    isVisibleDiaolog,
    setVisibleDiaolog,
    setDialogInputValue,
    keyboardType,
    makeUserInfoRequest,
  } = props;
  const token = useSelector(state => state.authentication.token);

  const makeRequest = async bodyContent => {
    const {result, error} = await updateUserInfoRequest(bodyContent, token);
    console.log(result);
    console.log(error);

    if (error) {
      showSnackBar(error.message, 'SHORT', 'ОК', () => {});
    }
  };
  return (
    <Dialog
      width={0.9}
      dialogTitle={<DialogTitle title={dialogTitle} />}
      visible={isVisibleDiaolog}
      dialogAnimation={
        new FadeAnimation({
          initialValue: 0,
          animationDuration: 150,
          useNativeDriver: true,
        })
      }
      onTouchOutside={() => {
        setVisibleDiaolog(false);
        setDialogInputValue(null);
      }}>
      <DialogContent>
        <TextInput
          style={styles.input}
          placeholder={'Введите данные'}
          value={dialogInputValue}
          onChangeText={setDialogInputValue}
          keyboardType={keyboardType}
        />
        <Button
          style={styles.button}
          disabled={!dialogInputValue}
          onPress={() => {
            makeRequest({id: id, [bodyField]: dialogInputValue});
            makeUserInfoRequest();
            setDialogInputValue(null);
            setVisibleDiaolog(false);
          }}>
          <Text style={styles.buttonText}>ОК</Text>
        </Button>
      </DialogContent>
    </Dialog>
  );
};

export default ChangeDialog;
