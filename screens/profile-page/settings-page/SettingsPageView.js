import React, {useState, useEffect} from 'react';
import {StyleSheet, TouchableHighlight} from 'react-native';
import {Screen, Text, Subtitle, Divider, Button, Icon, View} from '@shoutem/ui';
import {withRouter} from 'react-router-native';
import ChangeDialog from './ChangeDialog';

const styles = StyleSheet.create({
    screen: {padding: 10},
    text: {
        padding: 5,
    },
    devider: {
        borderColor: '#A6A6A6',
        marginTop: 5,
        marginBottom: 5,
    },
    button: {
        backgroundColor: '#FF5722',
        borderRadius: 30,
        height: 40,
        marginTop: 'auto',
        marginBottom: 10,
    },
    buttonText: {color: '#fff'},
    screenHeader: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    textView: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: 15,
        paddingRight: 15,
        marginTop: 5,
        marginBottom: 5,
    },
    subTitle: {
        fontSize: 20,
    },
    subValue: {
      fontSize: 18
    },
    subIcon: {
        paddingRight: 10,
        fontSize: 18,
    }
});
// updateUserInfoRequest
const SettingsPageView = props => {
    const {
        id,
        firstName,
        lastName,
        age,
        goalWeight,
        height,
        makeUserInfoRequest,
        history,
    } = props;
    const [popupTitle, setPopupTitle] = useState(null);
    const [isVisibleDiaolog, setVisibleDiaolog] = useState(false);
    const [dialogInputValue, setDialogInputValue] = useState(null);
    const [keyboardType, seteKyboardType] = useState('default');
    const [bodyField, setBodyField] = useState(null);
    return (
        <Screen style={styles.screen}>
            <View style={styles.screenHeader}>

                <Text style={{fontSize: 22}}>Настройки профиля</Text>
            </View>
            <Divider styleName="line" style={styles.devider}/>
            <TouchableHighlight
                style={styles.text}
                underlayColor="#A6A6A6"
                onPress={() => {
                    setPopupTitle('Задать имя');
                    seteKyboardType('default');
                    setVisibleDiaolog(true);
                    setBodyField('firstName');
                }}>
                <View style={styles.textView}>
                    <Subtitle style={styles.subTitle}><Icon style={styles.subIcon} name='edit'/>Имя: </Subtitle>
                    <Text style={styles.subValue}>{firstName}</Text>
                </View>
            </TouchableHighlight>
            <TouchableHighlight
                style={styles.text}
                underlayColor="#A6A6A6"
                onPress={() => {
                    setPopupTitle('Задать фамилию');
                    seteKyboardType('default');
                    setVisibleDiaolog(true);
                    setBodyField('lastName');
                }}>
                <View style={styles.textView}>
                    <Subtitle style={styles.subTitle}><Icon style={styles.subIcon} name='edit'/>Фамилия: </Subtitle>
                    <Text style={styles.subValue}>{lastName}</Text>
                </View>
            </TouchableHighlight>
            <TouchableHighlight
                style={styles.text}
                underlayColor="#A6A6A6"
                onPress={() => {
                    setPopupTitle('Задать возраст');
                    seteKyboardType('number-pad');
                    setVisibleDiaolog(true);
                    setBodyField('age');
                }}>
                <View style={styles.textView}>
                    <Subtitle style={styles.subTitle}><Icon style={styles.subIcon} name='edit'/>Возраст: </Subtitle>
                    <Text style={styles.subValue}>{age}</Text>
                </View>
            </TouchableHighlight>
            <TouchableHighlight
                style={styles.text}
                underlayColor="#A6A6A6"
                onPress={() => {
                    setPopupTitle('Задать желаемый вес');
                    seteKyboardType('number-pad');
                    setVisibleDiaolog(true);
                    setBodyField('goalWeight');
                }}>
                <View style={styles.textView}>
                    <Subtitle style={styles.subTitle}><Icon style={styles.subIcon} name='edit'/>Желаемый вес:</Subtitle>
                    <Text style={styles.subValue}>{goalWeight}</Text>
                </View>
            </TouchableHighlight>
            <TouchableHighlight
                style={styles.text}
                underlayColor="#A6A6A6"
                onPress={() => {
                    setPopupTitle('Задать рост');
                    seteKyboardType('number-pad');
                    setVisibleDiaolog(true);
                    setBodyField('height');
                }}>
                <View style={styles.textView}>
                    <Subtitle style={styles.subTitle}><Icon style={styles.subIcon} name='edit'/>Рост:</Subtitle>
                    <Text style={styles.subValue}>{height}</Text>
                </View>
            </TouchableHighlight>
            <ChangeDialog
                id={id}
                dialogTitle={popupTitle}
                dialogInputValue={dialogInputValue}
                isVisibleDiaolog={isVisibleDiaolog}
                setVisibleDiaolog={setVisibleDiaolog}
                setDialogInputValue={setDialogInputValue}
                keyboardType={keyboardType}
                bodyField={bodyField}
                makeUserInfoRequest={makeUserInfoRequest}
            />
            {/* <Button
        style={styles.button}
        onPress={() => {
          history.push('/');
        }}>
        <Text style={styles.buttonText}>Выйти из аккаунта</Text>
      </Button> */}
        </Screen>
    );
};

export default withRouter(SettingsPageView);
