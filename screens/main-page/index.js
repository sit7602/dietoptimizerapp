import React from 'react';
import {Modal, SafeAreaView, StyleSheet} from 'react-native';
import {Button, Icon, Screen, Text, TextInput, TouchableOpacity, View} from '@shoutem/ui';
import BottomNavigation from '../../components/bottom-navigation';
import CalorieProgressWidget from '../../components/calorie-progress-widget';
import ProductsHistory from './ProductsHistory';
import mealsRequest from '../../network/mealsRequest';
import {connect} from 'react-redux';
import moment from 'moment';
import 'moment/locale/ru';
import FAB from '../../components/floating-action-button-modal';
import userDietRequest from "../../network/userDietRequest";
import showSnackBar from "../../components/showSnackbar";
import {Autocomplete} from "react-native-dropdown-autocomplete";

const styles = StyleSheet.create({
  root: {backgroundColor: '#fff'},
  wrapper: {flex: 1},
  key: {
    fontSize: 40,
    fontWeight: 'bold',
  },
  key1: {
    fontSize: 40,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  value: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#FF5722',
  },
  input1: {width:'100%', marginBottom: 4, borderColor: 'black', borderWidth: 1, textAlign: 'center'},
  btnDefault: {
    opacity: 1,
    height: 50,
    borderRadius: 25,
    backgroundColor: '#FF5722',
  },
});

// const progressChartData = [0.4, 0.6, 0.8];
class MainPage extends React.Component {
  constructor(props) {
    super(props);
    moment.locale('ru');
    this.state = {
      token: '',
      userMeals: [],
      userMealsCached: [],
      isLoading: false,
      selectedDate: moment().startOf('day'),
      progress: 0,
      fat: 0,
      carb: 0,
      protein: 0,
      userDiet: undefined,
      goalWeight: undefined,
      noGoalWeight: false,
      goalWeightValue: 0,
      goalWeightReceived: false,
      userId: undefined,
      fatLimitPercent: 0,
      carbLimitPercent: 0,
      proteinLimitPercent: 0,
      caloriesLimit: 0,
    };
  };

  componentWillMount() {
    // console.ignoredYellowBox = true;
    // console.disableYellowBox = true;
    this.setState(
      {
        token: `Bearer_${this.props.token}`,
      },
      async () => {

        this.setState(
          {
            token: `Bearer_${this.props.token}`,
          },
          async () => {
            const response = await mealsRequest(this.state.token);
            console.log(response);
            if (response.result !== null) {
              // console.log(response.result[0].eaten_products)
              let progress = 0;
              let filtered_resp = [];
              let fat = 0;
              let carb = 0;
              let protein = 0;
              response.result.forEach(e => {
                if (this.state.selectedDate.isSame(moment(e.created), 'day')) {
                  filtered_resp.push(e);
                  progress += e.eaten_products.length * 5;
                  e.eaten_products.forEach(p => {
                    fat += p.product.fat;
                    carb += p.product.carb;
                    protein += p.product.protein;
                  });
                }
              });

              this.setState(
                {
                  userMeals: filtered_resp,
                  userMealsCached: response.result,
                  progress: progress,
                  fat: fat,
                  carb: carb,
                  protein: protein,
                },
                () => {
                  console.log(this.state.userMeals);
                  // this.setState({ isLoading: true, });
                },
              );
            } else {
              this.setState({
                // isLoading: true,
                userMeals:[],
                userMealsCached:[]})
            }
          },
        );
      },
    );
  };

  componentDidMount() {
    console.log('component did mount');
    this.getUserInfo();

  }

  clickFilterCallback = () => {
    let filteredUserMeals = [];
    let progress = 0;
    let fat = 0;
    let protein = 0;
    let carb = 0;

    let caloriesLimit = this.state.caloriesLimit;

    let fatLimitPercent = this.state.fatLimitPercent;
    let carbLimitPercent = this.state.carbLimitPercent;
    let proteinLimitPercent = this.state.proteinLimitPercent;
    let fatLimitGramm = caloriesLimit * fatLimitPercent / 9.290;
    let carbLimitGramm = caloriesLimit * carbLimitPercent / 4.100;
    let proteinLimitGramm = caloriesLimit * proteinLimitPercent / 4.100;
    let eatenCalories = 0;
    this.state.userMealsCached.forEach((e, i) => {
      if (this.state.selectedDate.isSame(moment(e.created), 'day')) {
        filteredUserMeals.push(e);
        e.eaten_products.forEach(p => {
          const productCalories = p.numberOfPortions * (p.product.calPer100Units * p.unitsInPortion / 100 /100);
          eatenCalories+= productCalories;
          fat += p.product.fat;
          carb += p.product.carb;
          protein += p.product.protein;
        });

      }
    });

    let calcProgress = eatenCalories/caloriesLimit > 1 ? 1 : eatenCalories/caloriesLimit;
    console.log(calcProgress);

    let stateFat = fat;
    if (typeof stateFat === 'string') {
      stateFat = parseInt(stateFat.split('/')[0])
    }

    let stateCarb = carb;
    if (typeof stateCarb === 'string') {
      stateCarb = parseInt(stateCarb.split('/')[0])
    }

    let stateProtein = protein;
    if (typeof stateProtein === 'string') {
      stateProtein = parseInt(stateProtein.split('/')[0])
    }

    this.setState({
      progress:Math.round(calcProgress * 100)/100 ,
      fat: `${stateFat}/${Math.round(fatLimitGramm*10)/10}`,
      carb: `${stateCarb}/${Math.round(carbLimitGramm*10)/10}`,
      protein: `${stateProtein}/${Math.round(proteinLimitGramm*10)/10}`,
      userMeals: filteredUserMeals,
    });

  };

  leftClick = () => {
    this.setState(
      {
        selectedDate: this.state.selectedDate.subtract(1, 'days').startOf('day'),
      },
      this.clickFilterCallback(),
    );
  };

  rightClick = () => {
    this.setState(
      {
        selectedDate: this.state.selectedDate.add(1, 'days'),
      },
      this.clickFilterCallback(),
    );
  };

  getUserDiet = async () => {
    let response = await userDietRequest(this.state.token);
    console.log('getUserDiet RESPONSE');
    console.log(response);
    if (response.error === null) {
      console.log(response.result);

      let caloriesLimit = response.result.userSpecific.calLimit;

      let fatLimitPercent = response.result.fat / 100;
      let carbLimitPercent = response.result.carb / 100;
      let proteinLimitPercent = response.result.protein / 100;
      let fatLimitGramm = caloriesLimit * fatLimitPercent / 9.290;
      let carbLimitGramm = caloriesLimit * carbLimitPercent / 4.100;
      let proteinLimitGramm = caloriesLimit * proteinLimitPercent / 4.100;
      let eatenCalories = 0;
      this.state.userMealsCached.forEach((e, i) => {
        if (this.state.selectedDate.isSame(moment(e.created), 'day')) {
          e.eaten_products.forEach(p => {
            // console.log('adsfdasf');
            // console.log(p.product.calPer100Units * p.unitsInPortion / 100);
            const productCalories = p.numberOfPortions * (p.product.calPer100Units * p.unitsInPortion / 100 /100);
            eatenCalories+= productCalories;
            // p.product.calPer100
            // fat += p.product.fat;
            // carb += p.product.carb;
            // protein += p.product.protein;
          });

        }
      });

      console.log('getUserDiet');

      console.log(caloriesLimit);
      console.log(eatenCalories);
      console.log(eatenCalories/caloriesLimit);
      let calcProgress = eatenCalories/caloriesLimit > 1 ? 1 : eatenCalories/caloriesLimit;
      console.log(calcProgress);


      let stateFat = this.state.fat;
      if (typeof stateFat === 'string') {
        stateFat = parseInt(stateFat.split('/')[0])
      }

      let stateCarb = this.state.carb;
      if (typeof stateCarb === 'string') {
        stateCarb = parseInt(stateCarb.split('/')[0])
      }

      let stateProtein = this.state.protein;
      if (typeof stateProtein === 'string') {
        stateProtein = parseInt(stateProtein.split('/')[0])
      }

      this.setState({
        fatLimitPercent: fatLimitPercent,
        carbLimitPercent: carbLimitPercent,
        proteinLimitPercent: proteinLimitPercent,
        caloriesLimit: caloriesLimit,
        progress:Math.round(calcProgress * 100)/100 ,
        fat: `${stateFat}/${Math.round(fatLimitGramm*10)/10}`,
        carb: `${stateCarb}/${Math.round(carbLimitGramm*10)/10}`,
        protein: `${stateProtein}/${Math.round(proteinLimitGramm*10)/10}`,
        userDiet: response.result,
        isLoading: true,

      });
      // progress: 0,
      //   fat: 0,
      //   carb: 0,
      //   protein: 0,
    } else {
      console.log('NO DIET');
      if (this.state.noGoalWeight === false) {
        this.props.history.push({
          pathname: '/diet',
        });
      }
    }
  };

  deleteMeal = (id,token) => {
    console.log(id, token);
    const host = 'http://tech.splinex-team.com:6667';
    const requestUrl = `${host}/api/v1/user/meal/${id}/delete`;
    console.log(requestUrl);
    let headers = {
      'Content-Type': 'application/json',
      Authorization: token,
    };
    console.log('HEDERAS');
    console.log(headers);
    fetch(requestUrl, {
      method: 'DELETE',
      headers: headers,
      mode: 'no-cors',
      cache: 'no-cache',
      redirect: 'manual',
    })
        .then(response => response.json())
        .then(resp => {
          console.log("DELETE MEAL RESP");
          console.log(resp);
          this.componentWillMount();
          this.componentDidMount();
        })
        .catch(error => {
          console.log("USDELET EMEAL RESPONSE");
          console.log(error);
          this.componentWillMount();
          this.componentDidMount();

          // this.props.history.push('/main-page');
        });
  };

  getUserInfo = () => {
    const host = 'http://tech.splinex-team.com:6667';
    const user_info_url = `${host}/api/v1/user/get-info`;
    let headers = {
      'Content-Type': 'application/json',
      Authorization: this.state.token,
    };
    console.log('HEADERS');
    console.log(headers);
    fetch(user_info_url, {
      method: 'GET',
      headers: headers,
      mode: 'no-cors',
      cache: 'no-cache',
      redirect: 'manual',
    })
      .then(response => response.json())
      .then(resp => {
        console.log("USER INFO RESPONSE");
        console.log(resp);
        if (resp.goalWeight === null) {
          console.log('noGoalWeight');
          this.setState({
            noGoalWeight:true,
            goalWeight: null,
            userId: resp.id,
            goalWeightReceived: true
          }, () => { this.getUserDiet() });
        } else {
          this.setState({
            noGoalWeight:false,
            goalWeight: resp.goalWeight,
            userId: resp.id,
            goalWeightReceived: true
          }, () => {this.getUserDiet()});
        }
      })
      .catch(error => {
        console.log("USER INFO RESPONSE");
        console.log(error);
      });
  };

  setGoalWeight = () => {
    console.log('setGoalWeight');
    console.log(Math.round(parseFloat(this.state.goalWeightValue)));

    const host = 'http://tech.splinex-team.com:6667';
    let headers = {
      'Content-Type': 'application/json',
      Authorization: this.state.token,
    };
    const data = {
      id: this.state.userId,
      goalWeight: Math.round(parseFloat(this.state.goalWeightValue))
    };
    fetch(
      `${host}/api/v1/user/update-info`,
      {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(data),
      },
    )
      .then(response => response.json())
      .then(resp => {
        console.log(resp);
        this.setState({noGoalWeight:false}, () => {this.getUserInfo()})

      })
      .catch(err => {
        console.log(err);
        showSnackBar('Ошибка добавления веса', 'SHORT', 'ОК', () => {});
      });
  };

  render() {
    // const { userDiet, token, goalWeight, noGoalWeight, goalWeightReceived } = this.state;
    //
    // if (goalWeight === undefined && token !== '') {
    //   this.getUserInfo();
    // }
    //
    // if (goalWeightReceived === true && noGoalWeight === false && userDiet === undefined && token !== '') {
    //   this.getUserDiet();
    // } else {
    //   console.log('skipping diet redirect');
    // }
    // const width = Dimensions.get("window").width;
    // const height = 220;
    return (
      <Screen style={styles.root}>
        <Modal
          style={{paddingTop: 50, }}
          animationType="slide"
          visible={this.state.noGoalWeight}
          transparent={true}
        >
          <View
            style={{
              backgroundColor: 'white',
              width: '100%',
              height: '100%',
              alignSelf: 'center',
              top: 0,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View style={{ width: '100%',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: 'white',
              marginBottom: 20,
              flexWrap: 'wrap',}}>
              <Text style={styles.key1}>Введите желаемый вес</Text>
              <TextInput ref={el => {this.input = el}}
                keyboardType={'numeric'}
                         onChangeText={(val)=>{this.setState({goalWeightValue: val}); console.log(val)}}
                         style={styles.input1}
                         placeholder={'Введите вес'}
              />
            </View>
            <View style={styles.section__sb}>
              <Button
                style={styles.btnDefault}
                stylename={'primary'}
                onPress={() => this.setGoalWeight()}>
                <Text style={{color: '#ffffff'}}>Сохранить</Text>
              </Button>
            </View>
          </View>
        </Modal>
        <View style={styles.wrapper}>
          <TouchableOpacity style={{flex: 3}}
                onPress={()=>{console.log('krug maksima'); this.props.history.push({
                  pathname: '/diet',
                });}}>
            <CalorieProgressWidget
                percent={this.state.progress * 1000}
                carb={this.state.carb}
                protein={this.state.protein}
                fat={this.state.fat}
                isLoaded={this.state.isLoading}
                onPress={()=>{console.log('krug maksima')}}
            />
          </TouchableOpacity>
          <View style={{flex: 5}}>
            <ProductsHistory
              style={{flex: 1, backgroundColor: 'magenta'}}
              title={'Products history'}
              isLoaded={this.state.isLoading}
              data={this.state.userMeals}
              leftClick={this.leftClick}
              rightClick={this.rightClick}
              selectedDate={this.state.selectedDate}
              deleteMeal={this.deleteMeal}
              token={this.state.token}
            />
          </View>
        </View>

        <FAB />
        <BottomNavigation />
      </Screen>
    );
  }
}

const mapStateToProps = state => ({
  token: state.authentication.token,
});

export default connect(mapStateToProps)(MainPage);
