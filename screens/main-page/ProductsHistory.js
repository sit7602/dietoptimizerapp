import React from 'react';
import {ActivityIndicator, ToastAndroid, StyleSheet, View} from 'react-native';
import {
  Button,
  Icon,
  ListView,
  Title,
  Subtitle,
  Caption,
  Tile,
  Divider,
  Text,
  TouchableOpacity,
} from '@shoutem/ui';
import moment from 'moment';
import {Link} from 'react-router-native';

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
  },
  listItemContainer: {
    width: '100%',
    flexDirection: 'column',
    alignItems: 'flex-end',
    paddingRight: 10,
  },
  caloriesHolder: {
    backgroundColor: 'rgba(120, 22, 22, 0.3)',
    width: 100,
    height: 30,
    borderColor: 'rgba(120, 22, 22, 0.4)',
    borderRadius: 35,
    alignItems: 'center',
    paddingTop: 5,
  },
  caloriesTextHolder: {
    paddingTop: 5,
    display: 'flex',
  },
  caloriesText: {
    fontSize: 16,
  },
  iconHolder: {
    paddingTop: 15,
    alignItems: 'center',
  },
  productRow: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    paddingHorizontal: 15,
    marginVertical: 7,
  },
  addToMealBtnView: {
    position: 'absolute',
    top: 7,
    right: 7,
    transform: [{ rotate: '45deg'}]
  },
  calendarHolder: {
    display: 'flex',
    flexDirection: 'row',
    paddingVertical: 10,
    backgroundColor: 'white',
    justifyContent: 'space-around',
    alignItems: 'center',
    width: '100%',
  },
  calendarText: {
    fontSize: 26,
  },
});

export default ProductsHistory = props => {
  const {title, data, isLoaded, leftClick, rightClick, selectedDate, deleteMeal, token} = props;

  const renderRow = data => {
    if (!data) {
      return (
        <Text
          style={{
            textTransform: 'uppercase',
            fontWeight: 'bold',
            fontSize: 30,
            textAlign: 'center',
            width: '100%',
            marginTop: 50,
          }}>
          No data
        </Text>
      );
    }
    console.log('MEAL1');
    console.log(data);
    return (
      <View style={{alignItems: 'center'}}>
        <View style={{width: '100%', paddingVertical: 5}}>
          <Title> {data.name} </Title>
          <Subtitle>
            {' '}
            {moment(data.created).format('DD-MMM-YY HH:MM')}{' '}
          </Subtitle>
          <View style={styles.addToMealBtnView}>
            <TouchableOpacity style={{flex: 1}} onPress={() => {
              console.log(`deleting ${data.id} token ${token}`);
              deleteMeal(data.id, token);}}>
              <Icon
                style={{color: 'red', fontSize: 40, zIndex: 2}}
                name="plus-button"
              />
            </TouchableOpacity>
          </View>
        </View>
        <Divider styleName="line" />
        <View style={{width: '100%'}}>
          <ListView
            style={{width: '100%', paddingHorizontal: 10, paddingLeft: 100}}
            data={data.eaten_products}
            renderRow={renderProduct}
          />
        </View>
        <Divider styleName="line" />
      </View>
    );
  };
  const renderProduct = product => {
    if (!product) {
      return (
        <Text
          style={{
            textTransform: 'uppercase',
            fontWeight: 'bold',
            fontSize: 30,
            textAlign: 'center',
            width: '100%',
            marginTop: 50,
          }}>
          Нет данных
        </Text>
      );
    }
      console.log('kek')
    console.log(product.product);
    let calories_total = Math.round(
      (product.product.calPer100Units / 100) * product.product.unitsPerPortion
    );
      console.log('CALORIES TOTAL = ', calories_total);
    calories_total = calories_total ? `${calories_total / 1000} кКал` : '0 кКал';
    let product_name =
      product.product.name.length > 18
        ? `${product.product.name.substring(0, 18)}...`
        : product.product.name;
    return (
      <View style={styles.productRow}>
        <View style={styles.caloriesTextHolder}>

          <Text style={styles.caloriesText}>{product.numberOfPortions} x {product_name}</Text>
        </View>
        <View style={styles.caloriesHolder}>
          <Text> {calories_total} </Text>
        </View>
      </View>
    );
  };


  if (!isLoaded) {
    return (
      <View style={styles.container}>
        <ActivityIndicator style={{paddingTop: 100}} size="large" />
      </View>
    );
  } else {
    if (!data.length) {
      return (
        <View style={styles.container}>
          <View style={styles.calendarHolder}>
            <Button styleName="clear" onPress={leftClick}>
              <Icon style={{fontSize: 40}} name="left-arrow" />
            </Button>
            <Title style={{paddingTop: 5}}>
              <Icon name="events" />
              <Text style={styles.calendarText}>
                {selectedDate.calendar(null, {
                  sameDay: '[Сегодня]',
                  nextDay: '[Завтра]',
                  nextWeek: 'DD.MM.YYYY',
                  lastDay: '[Вчера]',
                  lastWeek: 'DD.MM.YYYY',
                  sameElse: 'DD.MM.YYYY',
                })}
              </Text>
            </Title>
            <Button styleName="clear" onPress={rightClick}>
              <Icon style={{fontSize: 40}} name="right-arrow" />
            </Button>
          </View>
          <View style={{flex: 1, overflowY: 'scroll'}}>
            <Text
              style={{
                textTransform: 'uppercase',
                fontWeight: 'bold',
                fontSize: 30,
                textAlign: 'center',
                width: '100%',
                marginTop: 50,
              }}>
              Нет данных
            </Text>
          </View>
        </View>
      );
    } else {
      return (
        <View style={styles.container}>
          <View style={styles.calendarHolder}>
            <Button styleName="clear" onPress={leftClick}>
              <Icon style={{fontSize: 40}} name="left-arrow" />
            </Button>
            <Title style={{paddingTop: 5}}>
              <Icon name="events" />
              <Text style={styles.calendarText}>
                {selectedDate.calendar(null, {
                    sameDay: '[Сегодня]',
                    nextDay: '[Завтра]',
                    nextWeek: 'DD.MM.YYYY',
                    lastDay: '[Вчера]',
                    lastWeek: 'DD.MM.YYYY',
                    sameElse: 'DD.MM.YYYY',
                })}
              </Text>
            </Title>
            <Button styleName="clear" onPress={rightClick}>
              <Icon style={{fontSize: 40}} name="right-arrow" />
            </Button>
          </View>
          <View style={{flex: 1, overflowY: 'scroll'}}>
            <ListView style={{flex: 1}} data={data} renderRow={renderRow} />
          </View>
        </View>
      );
    }
  }
};
