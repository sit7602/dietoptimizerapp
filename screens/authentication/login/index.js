import React, {useState} from 'react';
import LoginView from './LoginView';
import {authenticate} from '../../../redux/actions/authenticate';
import {useSelector, useDispatch} from 'react-redux';
import {Redirect} from 'react-router-native';
import showSnackBar from '../../../components/showSnackbar';

const Login = () => {
  const [loginInput, setLoginInput] = useState('');
  const [passwordInput, setpasswordInput] = useState('');
  const dispatch = useDispatch();
  const isLoading = useSelector(state => state.authentication.isLoading);
  const token = useSelector(state => state.authentication.token);
  const isAuthenticated = useSelector(
    state => state.authentication.isAuthenticated,
  );

  const onPress = () => {
    if (!loginInput || !passwordInput) {
      showSnackBar('Заполните все поля', 'SHORT', 'ОК', () => {});
      return;
    }
    dispatch(authenticate(loginInput, passwordInput));
  };

  return token ? (
    <Redirect to="/main" />
  ) : (
    <LoginView
      onLoginInputChange={setLoginInput}
      onPasswordInputChange={setpasswordInput}
      loginValue={loginInput}
      passwordValue={passwordInput}
      onPress={onPress}
      isLoading={isLoading}
    />
  );
};

export default Login;
