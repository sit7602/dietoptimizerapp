import React from 'react';
import {StyleSheet} from 'react-native';
import {Button, Text, Spinner} from '@shoutem/ui';

const styles = StyleSheet.create({
  button: {
    backgroundColor: '#FF5722',
    marginTop: 5,
    marginBottom: 5,
    borderRadius: 30,
    width: 340,
    height: 40,
  },
  buttonSmall: {
    backgroundColor: '#FF5722',
    marginTop: 5,
    marginBottom: 5,
    borderRadius: 30,
    width: 150,
    height: 40,
  },
  buttonText: {color: '#fff'},
  spinner: {
    color: '#fff',
  },
});

const LoadingButton = props => {
  const {buttonText, onPress, isLoading, size} = props;
  const buttonContent = isLoading ? (
    <Spinner style={styles.spinner} />
  ) : (
    <Text style={styles.buttonText}>{buttonText}</Text>
  );
  let btnStyle = size ? styles.buttonSmall : styles.button;
  return (
    <Button style={btnStyle} onPress={onPress} disabled={isLoading}>
      {buttonContent}
    </Button>
  );
};

export default LoadingButton;
