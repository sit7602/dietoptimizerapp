import React from 'react';
import FAB from 'react-native-fab';
import {Link} from 'react-router-native';
import {ToastAndroid, StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  link: {
    flex: 0.05,
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
  },
});

const FloatingNavButton = props => {
  const {visible, color, to} = props;
  return (
    <Link
      style={styles.link}
      to={to}
      onPress={() => ToastAndroid.show('click link', ToastAndroid.SHORT)}>
      <FAB buttonColor={color} visible={visible} />
    </Link>
  );
};

export default FloatingNavButton;
