import React from 'react';
import {StyleSheet} from 'react-native';
import ActionButton from 'react-native-action-button';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import {withRouter} from 'react-router-native';

const FloatingActionButtonModal = ({history}) => {
  return (
    <ActionButton buttonColor="#FF5722" offsetY={65} offsetX={10}>
      <ActionButton.Item
        buttonColor="#fff"
        title="Через камеру"
        onPress={() => {
          history.push('/camera');
        }}>
        <EntypoIcon name="camera" style={styles.actionButtonIcon} />
      </ActionButton.Item>
      <ActionButton.Item
        buttonColor="#fff"
        title="Вручную/Из диеты"
        onPress={() => {
          history.push('/product-form');
        }}>
        <MaterialIcon
          name="food-fork-drink"
          style={styles.actionButtonIconDiet}
        />
      </ActionButton.Item>
    </ActionButton>
  );
};

const styles = StyleSheet.create({
  actionButtonIconCamera: {
    fontSize: 20,
    height: 22,
    color: '#3498db',
  },
  actionButtonIconDiet: {
    fontSize: 20,
    height: 22,
    color: '#1abc9c',
  },
});

export default withRouter(FloatingActionButtonModal);
