import React from 'react';
import {LineChart} from 'react-native-chart-kit';
import {Dimensions} from 'react-native';
const screenWidth = Dimensions.get('window').width;

const Graphics = props => {
  const {graphData} = props;
  console.log("graphData");
  console.log(graphData);
  console.log(graphData.datasets[0].data);

  return (
    <LineChart
      data={graphData}
      width={screenWidth}
      height={160}
      chartConfig={{
        backgroundGradientFrom: '#f2f2f2',
        backgroundGradientTo: '#f2f2f2',
        decimalPlaces: 2,
        color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
        style: {
          borderRadius: 16,
        },
      }}
    />
  );
};

export default Graphics;
