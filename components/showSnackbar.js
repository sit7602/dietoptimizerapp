import {WSnackBar} from 'react-native-smart-tip';

const showSnackBar = (text, duration, actionText, actionClick) => {
  const snackBarOpts = {
    data: `${text}`,
    position: WSnackBar.position.BOTTOM, // 1.TOP 2.CENTER 3.BOTTOM
    duration: WSnackBar.duration[duration], //1.SHORT 2.LONG 3.INDEFINITE
    textColor: '#FF5722',
    backgroundColor: '#050405',
    actionText: actionText,
    actionTextColor: '#FF5722',
    actionClick: actionClick,
  };

  WSnackBar.show(snackBarOpts);
};

export default showSnackBar;
