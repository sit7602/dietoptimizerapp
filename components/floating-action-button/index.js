import React from 'react';
import {StyleSheet} from 'react-native';
import {Link} from 'react-router-native';
import {Icon} from '@shoutem/ui';
const styles = StyleSheet.create({
  button: {},
  link: {
    backgroundColor: '#FF5722',
    width: 60,
    height: 60,
    borderRadius: 30,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.3,
    shadowRadius: 4.65,
    elevation: 8,
    position: 'absolute',
    right: 10,
    bottom: 65,
    alignItems: 'center',
    justifyContent: 'center',
  },
  icon: {
    color: '#fff',
    fontSize: 35,
  },
});

const FloatingActionButton = props => {
  const {to} = props;
  return (
    <Link style={styles.link} to={to} underlayColor="#D9491C">
      <Icon style={styles.icon} name="plus-button" />
    </Link>
  );
};

export default FloatingActionButton;
