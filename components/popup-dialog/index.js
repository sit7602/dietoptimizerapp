import React from 'react';
import {StyleSheet} from 'react-native';
import Dialog, {
  FadeAnimation,
  DialogContent,
  DialogTitle,
} from 'react-native-popup-dialog';
import {Button, Text, TextInput} from '@shoutem/ui';

const styles = StyleSheet.create({
  button: {
    backgroundColor: '#FF5722',
    borderRadius: 30,
    width: 130,
    height: 40,
    marginTop: 10,
  },
  buttonText: {color: '#fff'},
});
const PopupDialog = props => {
  const {
    dialogContent,
    dialogTitle,
    isVisibleDiaolog,
    setVisibleDiaolog,
    setDialogInputValue,
  } = props;
  return (
    <Dialog
      width={0.9}
      dialogTitle={<DialogTitle title={dialogTitle} />}
      visible={isVisibleDiaolog}
      dialogAnimation={
        new FadeAnimation({
          initialValue: 0,
          animationDuration: 150,
          useNativeDriver: true,
        })
      }
      onTouchOutside={() => {
        setVisibleDiaolog(false);
        setDialogInputValue(null);
      }}>
      <DialogContent>{dialogContent}</DialogContent>
    </Dialog>
  );
};

export default PopupDialog;
