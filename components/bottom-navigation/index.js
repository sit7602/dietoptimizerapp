import React from 'react';
import {StyleSheet} from 'react-native';
import {View} from '@shoutem/ui';
import NavButton from './NavButton';
import {withRouter} from 'react-router-native';

const styles = StyleSheet.create({
  root: {
    backgroundColor: '#fff',
    flexDirection: 'row',
    justifyContent: 'space-around',
    height: 55,
    borderTopColor: '#F1F1F1',
    borderTopWidth: 1,
  },
});

const BottomNavigation = ({location}) => {
  return (
    <View style={styles.root}>
      <NavButton
        to={'/main'}
        iconName={'equalizer'}
        isActive={location.pathname === '/main'}
      />
      <NavButton
        to={'/diet'}
        iconName={'restaurant'}
        isActive={location.pathname === '/diet'}
      />
      <NavButton
        to={'/profile'}
        iconName={'user-profile'}
        isActive={location.pathname === '/profile'}
      />
    </View>
  );
};

export default withRouter(BottomNavigation);
