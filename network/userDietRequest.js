const userDietRequest = async (token) => {
    try {
        const url = `http://tech.splinex-team.com:6667/api/v1/user/diet/get`;

        const response = await fetch(
            `${url}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: token,
                },
            }
        );

        if (!response.ok) {
            const error = await response.json();
            return {result: null, error: error};
        }

        const result = await response.json();
        return {result: result, error: null};

    } catch (error) {
        return {result: null, error: error};
    }
};

export default userDietRequest;
