const registrationRequest = async (username, password, email, firstName, lastName, age, weight, height, sex) => {
    try {
        const response = await fetch(
            'http://tech.splinex-team.com:6667/api/v1/auth/register',
            {
                method: 'POST',
                body: JSON.stringify({
                    username: username,
                    password: password,
                    email: email,
                    firstName: firstName,
                    lastName: lastName,
                    age: age,
                    weight: weight,
                    height: height,
                    sex: sex,
                }),
                headers: {
                    'Content-Type': 'application/json',
                },
            },
        );

        if (!response.ok) {
            const error = await response.json();
            return {result: null, error};
        }

        const result = await response.json();
        return {result, error: null};
    } catch (error) {
        return {result: null, error};
    }
};

export default registrationRequest;
