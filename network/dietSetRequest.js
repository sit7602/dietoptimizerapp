const dietSetRequest = async (token, id) => {
    try {
        const url = `http://tech.splinex-team.com:6667/api/v1/user/diet/set?id=${id}`;

        const response = await fetch(
            `${url}`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: token,
                },
                body: JSON.stringify({
                    id: parseInt(id),
                })
            }
        );

        if (!response.ok) {
            const error = await response.json();
            return {result: null, error: error};
        }

        const result = await response.json();
        return {result: result, error: null};

    } catch (error) {
        return {result: null, error: error};
    }
};

export default dietSetRequest;
