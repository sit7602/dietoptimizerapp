const authenticationRequest = async (login, password) => {
  try {
    const response = await fetch(
      'http://tech.splinex-team.com:6667/api/v1/auth/login',
      {
        method: 'POST',
        body: JSON.stringify({
          username: login,
          password,
        }),
        headers: {
          'Content-Type': 'application/json',
        },
      },
    );

    if (!response.ok) {
      const error = await response.json();
      return {result: null, error};
    }

    const result = await response.json();
    return {result, error: null};
  } catch (error) {
    return {result: null, error};
  }
};

export default authenticationRequest;
