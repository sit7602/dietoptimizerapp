const addProductRequest = async (name, category, calPer100Units, protein, fat, carb, barcode) => {
    try {
        const response = await fetch(
            'http://tech.splinex-team.com:6667/api/v1/user/product/add',
            {
                    method: 'POST',
                    body: JSON.stringify({
                        name: name,
                        category: category,
                        calPer100Units: calPer100Units,
                        protein: protein,
                        fat: fat,
                        carb: carb,
                        barcode: barcode,
                    }),
                    headers: {
                        'Content-Type': 'application/json',
                    },
                },
        );
        if (!response.ok) {
            const error = await response.json();
            return { result: null, error };
        }

        const result = await response.json();
        return { result, error: null };
    } catch (error) {
        return { result: null, error };
    }
};

export default addProductRequest;
