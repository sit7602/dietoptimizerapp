const weightRequest = async (method, type, token, addValue) => {
  try {
    const response = await fetch(
      `http://tech.splinex-team.com:6667/api/v1/user/weight/${type}`,
      {
        method: method,
        body: addValue
          ? JSON.stringify({
              value: addValue,
            })
          : null,
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer_${token.replace('Bearer_','')}`,
        },
      },
    );

    if (!response.ok) {
      const error = await response.json();
      return {result: null, error};
    }

    const result = await response.json();
    return {result, error: null};
  } catch (error) {
    return {result: null, error};
  }
};

export default weightRequest;
